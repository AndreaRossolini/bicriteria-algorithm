\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {1.1}Mathematical formulation}{4}
\contentsline {chapter}{\numberline {2}Mono-criteria algorithms}{6}
\contentsline {section}{\numberline {2.1}Dijkstra's algorithms}{6}
\contentsline {subsection}{\numberline {2.1.1}One to all}{6}
\contentsline {subsection}{\numberline {2.1.2}One to one}{6}
\contentsline {subsection}{\numberline {2.1.3}List of candidate}{7}
\contentsline {section}{\numberline {2.2}``A Star'' algorithm}{7}
\contentsline {section}{\numberline {2.3}Analysis of the results}{8}
\contentsline {subsection}{\numberline {2.3.1}Performance comparison}{8}
\contentsline {subsection}{\numberline {2.3.2}Node visited}{10}
\contentsline {chapter}{\numberline {3}Bicriteria algorithms}{14}
\contentsline {section}{\numberline {3.1}Dijkstra applied to bicriteria}{14}
\contentsline {subsection}{\numberline {3.1.1}Application of Bicriteria Dijkstra}{15}
\contentsline {subsubsection}{Bicriteria Dijkstra iteration}{15}
\contentsline {subsubsection}{Bicriteria Dijkstra with binary research}{16}
\contentsline {section}{\numberline {3.2}Label-setting algorithm}{17}
\contentsline {subsection}{\numberline {3.2.1}Implementation}{17}
\contentsline {subsection}{\numberline {3.2.2}Lower bound improvement}{19}
\contentsline {subsection}{\numberline {3.2.3}Complexity}{20}
\contentsline {section}{\numberline {3.3}Analysis of results}{21}
\contentsline {section}{\numberline {3.4}Bidirectional Bi-criteria Algorithm}{23}
\contentsline {subsection}{\numberline {3.4.1}Implementation}{23}
\contentsline {subsubsection}{Particular Stop Condition Case}{23}
\contentsline {subsection}{\numberline {3.4.2}Performance comparison}{23}
\contentsline {subsection}{\numberline {3.4.3}Node visited}{24}
\contentsline {chapter}{\numberline {4}Implementation details}{29}
\contentsline {section}{\numberline {4.1}Python}{29}
\contentsline {subsection}{\numberline {4.1.1}Binary queue}{29}
\contentsline {section}{\numberline {4.2}Nodes as objects}{29}
\contentsline {section}{\numberline {4.3}Pandas \& Matplotlib}{30}
\contentsline {section}{\numberline {4.4}Testing}{31}
\contentsline {chapter}{\numberline {5}Personal considerations}{32}
\contentsline {section}{\numberline {5.1}Other possible solution}{32}
\contentsline {section}{\numberline {5.2}Other possible application}{33}
\contentsline {section}{\numberline {5.3}Difficulties encountered}{33}
\contentsline {section}{\numberline {5.4}All not-standard python's libraries used:}{33}
\contentsline {section}{\numberline {5.5}Computer info}{34}
