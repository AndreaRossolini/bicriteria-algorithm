\babel@toc {italian}{}
\babel@toc {italian}{}
\contentsline {chapter}{\numberline {1}Introduzione}{1}
\contentsline {section}{\numberline {1.1}Formulazione Matematica}{3}
\contentsline {chapter}{\numberline {2}Algoritmi `Mono-criteria'}{5}
\contentsline {section}{\numberline {2.1}Algoritmo di Dijkstra}{5}
\contentsline {subsection}{\numberline {2.1.1}One to All}{5}
\contentsline {subsection}{\numberline {2.1.2}One to One}{7}
\contentsline {subsection}{\numberline {2.1.3}List of Candidate}{7}
\contentsline {section}{\numberline {2.2}``A Star''}{7}
\contentsline {section}{\numberline {2.3}Analisi dei Risultati}{10}
\contentsline {subsection}{\numberline {2.3.1}Performance a Confronto}{10}
\contentsline {subsection}{\numberline {2.3.2}Nodi Visitati}{12}
\contentsline {chapter}{\numberline {3}Algoritmi `Bi-criteria'}{17}
\contentsline {section}{\numberline {3.1}Dijkstra applicato ad un problema Bi-criteria}{18}
\contentsline {subsection}{\numberline {3.1.1}Implementazioni dell'algoritmo di Dijkstra con due criteri}{19}
\contentsline {subsubsection}{Iterazione Dijkstra bi-criteria}{20}
\contentsline {subsubsection}{Dijkstra bi-criteria con ricerca binaria}{20}
\contentsline {section}{\numberline {3.2}Algoritmo di Label-setting}{22}
\contentsline {subsection}{\numberline {3.2.1}Implementazione}{23}
\contentsline {subsubsection}{Complessit\`a computazionale}{25}
\contentsline {section}{\numberline {3.3}Algoritmo di Label-setting con minoranti pregressi}{26}
\contentsline {subsection}{\numberline {3.3.1}Algoritmo di label-setting con minoranti pregressi invertito}{26}
\contentsline {section}{\numberline {3.4}Algoritmo Bidirezionale Bi-criteria}{27}
\contentsline {subsection}{\numberline {3.4.1}Implementazione}{29}
\contentsline {subsubsection}{Caso particolare della condizione di stop}{29}
\contentsline {section}{\numberline {3.5}Analisi dei risultati}{32}
\contentsline {subsection}{\numberline {3.5.1}Performance a confronto}{32}
\contentsline {subsection}{\numberline {3.5.2}Nodi visitati}{35}
\contentsline {chapter}{\numberline {4}Considerazioni Personali}{41}
\contentsline {section}{\numberline {4.1}Idee Scartate}{41}
\contentsline {section}{\numberline {4.2}Possibili Applicazioni}{42}
\contentsline {section}{\numberline {4.3}Difficolt\`a incontrate}{42}
\contentsline {section}{\numberline {4.4}Sviluppi futuri}{43}
\contentsline {chapter}{\numberline {5}Conclusioni}{45}
\contentsline {chapter}{\numberline {6}Dettagli implementativi}{47}
\contentsline {section}{\numberline {6.1}Informazioni sui grafi}{47}
\contentsline {section}{\numberline {6.2}Informazioni sulla macchina utilizzata}{48}
